/**
 * Created by jake on 6/30/14.
 */
import java.util.Arrays;
public class Fibonacci  {
	private int count = 2;
	private int[] seq;

	Fibonacci(int max){
		seq = new int[max];
		seq[0] = 1;
		seq[1] = 1;
	}

	public boolean hasNext(){
		return count < seq.length;
	}

	public int next() {
		seq[count] = seq[count - 2] + seq[count - 1];
		count++;
		return seq[count - 1];

	}
	public void remove(){

	}
	public void fillArray(){
		for (int i = 0; i < count; i++){
			while (hasNext()){
				next();
			}
		}
	}
	public int[] getSeq(){
		return seq;
	}
	public static void main(String[] args){
		Fibonacci fib = new Fibonacci(43);
		fib.fillArray();
		System.out.println(Arrays.toString(fib.getSeq()));
	}
}
